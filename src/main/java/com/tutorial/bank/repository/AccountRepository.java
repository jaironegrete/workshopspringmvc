package com.tutorial.bank.repository;

import com.tutorial.bank.api.dto.AccountDTO;
import com.tutorial.bank.model.Account;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface AccountRepository extends CrudRepository<Account, String> {

    List<Account> findByHolderNameStartingWith(String holderName);

    @Query("SELECT new com.tutorial.bank.api.dto.AccountDTO(a.holderName) FROM Account a WHERE a.holderName like :holderName")
    List<AccountDTO> searchByHolderName(@Param("holderName") String name);

    List<Account> findByHolderNameContains(String holderName);
}
