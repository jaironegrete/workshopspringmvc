package com.tutorial.bank.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Entity
public class Account {

    @Id
    String number;

    @NotNull
    String holderName;

    @Column(scale = 2)
    BigDecimal summary;

    public Account () {
    }

    public Account(String number, @NotNull String holderName, BigDecimal summary) {
        this.number = number;
        this.holderName = holderName;
        this.summary = summary;
    }

    public String getNumber() {
        return number;
    }

    public String getHolderName() {
        return holderName;
    }

    public BigDecimal getSummary() {
        return summary;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public void setSummary(BigDecimal summary) {
        this.summary = summary;
    }
}
