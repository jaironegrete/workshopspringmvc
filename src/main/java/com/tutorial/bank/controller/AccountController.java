package com.tutorial.bank.controller;

import com.tutorial.bank.api.dto.AccountDTO;
import com.tutorial.bank.api.operation.AccountOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RestController
@RequestMapping("api")
public class AccountController {

    @Autowired
    private AccountOperation accountOperation;

    AccountController(AccountOperation accountOperation){
        this.accountOperation = accountOperation;
    }

    @PostMapping("/account")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public void saveAccount(@RequestBody @Validated AccountDTO accountDTO){
        this.accountOperation.createAccount(accountDTO);
    }

    @PutMapping("/account/{number}")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public void addCreditToAccount(@PathVariable String number, @RequestBody String value) {
        this.accountOperation.addCreditToSummary(number, new BigDecimal(value));
    }

    @GetMapping("/health")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String addCreditToAccount() {
        return "up";
    }
}
