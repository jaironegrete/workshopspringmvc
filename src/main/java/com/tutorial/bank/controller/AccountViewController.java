package com.tutorial.bank.controller;

import com.tutorial.bank.api.dto.AccountDTO;
import com.tutorial.bank.api.operation.AccountOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.view.RedirectView;

import java.math.BigDecimal;

@Controller
public class AccountViewController {

    @Autowired
    private AccountOperation accountOperation;


    @GetMapping("/account")
    public String getNewAccount(Model model) {
        model.addAttribute("account", new AccountDTO());
        return "createAccount";
    }

    @PostMapping("/account")
    public RedirectView saveNewAccount(@ModelAttribute AccountDTO accountDTO) {
        accountOperation.createAccount(accountDTO);
        return new RedirectView("/bank/account");
    }

    @GetMapping("/account/summary/{number}")
    public String changeSummaryValue(@PathVariable String number, Model model) {
        AccountDTO accountDTO = accountOperation.findAccountDtoById(number);
        model.addAttribute("account", accountDTO);
        return "changeSummary";
    }

    @PostMapping("/account/summary")
    public RedirectView changeSummaryValue(@ModelAttribute BigDecimal value) {
        return new RedirectView("/bank/changeSummary");
    }
}