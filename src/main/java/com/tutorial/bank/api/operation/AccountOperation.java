package com.tutorial.bank.api.operation;

import com.tutorial.bank.api.dto.AccountDTO;
import com.tutorial.bank.model.Account;

import java.math.BigDecimal;

public interface AccountOperation {
    void createAccount(AccountDTO accountDTO);
    void addCreditToSummary(String number, BigDecimal value);
    AccountDTO findAccountDtoById(String number);
    Account findById(String number);
}
