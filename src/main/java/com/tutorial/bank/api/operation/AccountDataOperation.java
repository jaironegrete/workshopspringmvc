package com.tutorial.bank.api.operation;

import com.tutorial.bank.api.dto.AccountDTO;
import com.tutorial.bank.model.Account;

import java.util.List;

public interface AccountDataOperation {

    void saveNewAccount(AccountDTO account);
    void delete(String id);
    List<Account> findByHolderNameStartingWith(String holderName);
    List<AccountDTO> searchByHolderName(String holderName);
    List<Account> findByHolderNameContains(String holderName);
    AccountDTO findAccountDtoById(String id);
    void save(Account account);
    Account findById(String id);
}
