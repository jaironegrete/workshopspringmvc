package com.tutorial.bank.api.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

public class AccountDTO implements Serializable {

    @NotNull
    String number;

    @NotNull
    String holderName;

    BigDecimal summary;

    public AccountDTO(){

    }


    public AccountDTO(String holderName) {
        this.holderName = holderName;
    }

    public AccountDTO(String holderName, String number) {
        this.holderName = holderName;
        this.number = number;
    }

    public void setSummary(BigDecimal summary) {
        this.summary = summary;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public String getHolderName() {
        return holderName;
    }

    public BigDecimal getSummary() {
        return summary;
    }
}
