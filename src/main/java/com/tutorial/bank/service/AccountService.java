package com.tutorial.bank.service;

import com.tutorial.bank.api.dto.AccountDTO;
import com.tutorial.bank.api.operation.AccountDataOperation;
import com.tutorial.bank.api.operation.AccountOperation;
import com.tutorial.bank.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class AccountService implements AccountOperation {

    @Autowired
    private AccountDataOperation accountOperation;

    AccountService (AccountDataOperation accountOperation){
        this.accountOperation = accountOperation;
    }

    public void createAccount(AccountDTO accountDTO){
        accountDTO.setSummary(new BigDecimal(0));
        this.accountOperation.saveNewAccount(accountDTO);
    }

    @Override
    public void addCreditToSummary(String number, BigDecimal value) {
        Account account = accountOperation.findById(number);
        account.setSummary(account.getSummary().add(value));
        accountOperation.save(account);
    }

    @Override
    public AccountDTO findAccountDtoById(String number) {
        return accountOperation.findAccountDtoById(number);
    }

    @Override
    public Account findById(String number) {
        return accountOperation.findById(number);
    }
}
