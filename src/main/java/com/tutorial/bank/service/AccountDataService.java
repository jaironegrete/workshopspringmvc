package com.tutorial.bank.service;

import com.tutorial.bank.api.dto.AccountDTO;
import com.tutorial.bank.api.operation.AccountDataOperation;
import com.tutorial.bank.model.Account;
import com.tutorial.bank.repository.AccountRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountDataService implements AccountDataOperation {

    @Autowired
    private AccountRepository accountRepository;

    AccountDataService (AccountRepository accountRepository){
        this.accountRepository = accountRepository;
    }

    @Override
    public void saveNewAccount(AccountDTO accountDTO) {
        Account newAccount = new Account();
        if (!accountRepository.existsById(accountDTO.getNumber())) {
            BeanUtils.copyProperties(accountDTO, newAccount);
            this.accountRepository.save(newAccount);
        }
    }

    @Override
    public void save(Account account) {
        this.accountRepository.save(account);
    }

    public List<Account> findByHolderNameContains(String holderName) {
        return this.accountRepository.findByHolderNameContains(holderName);
    }

    @Override
    public void delete(String id) {
        this.accountRepository.deleteById(id);
    }

    @Override
    public List<Account> findByHolderNameStartingWith(String holderName) {
        return this.accountRepository.findByHolderNameStartingWith(holderName);
    }

    @Override
    public AccountDTO findAccountDtoById(String id) {
        Account account = this.accountRepository.findById(id).orElse(null);
        AccountDTO accountDTO = new AccountDTO();
        BeanUtils.copyProperties(account, accountDTO);
        return accountDTO;
    }

    @Override
    public Account findById(String id) {
        return this.accountRepository.findById(id).orElse(null);
    }

    @Override
    public List<AccountDTO> searchByHolderName(String holderName) {
        return this.accountRepository.searchByHolderName(holderName);
    }
}
